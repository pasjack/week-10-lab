from turtle import *


def setup():
    #draws hanging pole
    forward(100)
    left(180)
    forward(50)
    right(90)
    forward(200)
    right(90)
    forward(75)
    right(90)
    forward(25)
    penup()


def head():
    pendown()
    #draws head
    right(90)
    circle(20)

    #moves to begining of neck
    circle(20,180)
    right(90)
    penup()


def body():
    pendown()
    forward(75)
    penup()


def left_leg():
    pendown()
    right(45)
    forward(50)
    left(180)
    forward(50)
    right(135)
    penup()


def right_leg():
    pendown()
    left(45)
    forward(50)
    left(180)
    forward(50)
    right(45)
    forward(50)
    penup()


def left_arm():
    pendown()
    left(135)
    forward(50)
    right(180)
    forward(50)
    right(135)
    penup()


def right_arm():
    pendown()
    left(45)
    forward(50)
    right(180)
    forward(50)
    penup()


def win():
    goto(0,0)
    pendown()
    write("You Win", font=("Arial", 12, "normal"))
    penup()


def lose():
    goto(0,0)
    pendown()
    write("You Lost", font=("Arial", 12, "normal"))
    penup()


def print_blank_list(blank_list):
    print()
    for i in blank_list:
        print(i, end="")
    print()

def print_guess_list(guess_list):
    print()
    print("Guesses:")
    for i in guess_list:
        if i == guess_list[-1]:
            print(i)
        else:
            print(i, end=", ")


def get_phrase():
    phrase = input("Enter the word to be guessed: ").lower()
    phrase_list = []
    blank_list = []
    for letter in phrase:
        phrase_list.append(letter)
        if letter == " ":
            blank_list.append(" ")
        else:
            blank_list.append("-")

    return blank_list, phrase_list, phrase



def main():
    speed(0)
    hideturtle()
    setup()

    blank_list, phrase_list, phrase = get_phrase()
    print_blank_list(blank_list)

    guess_list = []
    guess = input("Enter a letter to guess: ").lower()
    i = 0
    while True:
        if guess == "":
            break
        if guess in guess_list:
            print("\nYou already guessed that\n")
            print_guess_list(guess_list)
            print_blank_list(blank_list)
            guess = input("Enter a letter to guess: ").lower()
            continue

        guess_list.append(guess)
        if guess in phrase_list:
            count_many = phrase.count(guess)
            y = 0
            for j in range(count_many):
                x = phrase.find(guess, y)
                y = 1 + x
                blank_list[x] = guess

            if blank_list == phrase_list:
                win()
                print("\nYou Won\n")
                break

            print_guess_list(guess_list)
            print_blank_list(blank_list)

        else:
            i += 1
            print(i)
            if i == 1:
                head()
            elif i == 2:
                body()
            elif i == 3:
                left_leg()
            elif i == 4:
                right_leg()
            elif i == 5:
                left_arm()
            else:
                right_arm()
                lose()
                print("\nYou Lost\n")
                break

            print_guess_list(guess_list)
            print_blank_list(blank_list)


        guess = input("Enter a letter to guess: ").lower()

    print("All Finished")

    done()

if __name__ == "__main__":
    main()